require('../common/common.js');
require('breakpoints-js/dist/breakpoints.min.js');
require('../numeric-input/_numeric-input.js');
require('select2');


import './index.scss';

$(function () {
	$('.order-form__field > select').select2({
		minimumResultsForSearch: -1
	});
});

