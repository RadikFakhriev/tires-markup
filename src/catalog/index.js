require('../common/common.js');
require('breakpoints-js/dist/breakpoints.min.js');
require('../numeric-input/_numeric-input.js');
require('select2');
require('jquery.scrollbar');


import './index.scss';

$(function () {

	$('#SubCategoryTabs a').on('shown.bs.tab', function (event) {
		var tabHash = event.currentTarget.hash;
		switch (tabHash) {
			case '#cargo':
				$('.nav-link[href="#cargo"]').parent('.nav-item').addClass('active-tab');
				break;
			case '#specTires':
				$('.nav-link[href="#specTires"]').parent('.nav-item').addClass('active-tab');
				break;
		}

		$(this).closest('.nav-item').siblings().removeClass('active-tab');
	});

	$('#ProductsFilter').find('.list-group-item').first().find('.filter-group-content').one('shown.bs.collapse', function (event) {
		$(this).find('.filter-props-list').scrollbar();
	});


	$("#toTop").click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});
});

