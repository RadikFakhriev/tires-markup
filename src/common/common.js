// Bootstrap dependencies
window.$ = window.jQuery = require('jquery') // required for bootstrap
window.Popper = require('popper.js') // required for tooltip, popup...
require('bootstrap');

// tooltip and popover require javascript side modification to enable them (new in Bootstrap 4)
// use tooltip and popover components everywhere
$(function (){
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();

	Breakpoints();

	Breakpoints.on('xs sm', {
		enter: function() {
			$('#NaviDropdown').find('.navbar-nav').removeClass('row page-centerer');
		},
		leave: function() {
			$('#NaviDropdown').find('.navbar-nav').addClass('row page-centerer');
			$('#NaviDropdown').collapse('hide');
		}
	});

	$('#NaviDropdown').on('show.bs.collapse', function () {
		$('.sidebar-overlay').addClass('active');
		$('body').addClass('unscrollable');
	});

	$('#NaviDropdown').on('hide.bs.collapse', function () {
		$('.sidebar-overlay').removeClass('active');
		$('body').removeClass('unscrollable');
	});

	// $('.sidebar-overlay').on('click', function () {
	// 	$('#NaviDropdown').collapse('hide');
	// });
});


// String.prototype.includes() polyfill
if (!String.prototype.includes) {
	String.prototype.includes = function(search, start) {
		'use strict';
		if (typeof start !== 'number') {
			start = 0;
		}
		
		if (start + search.length > this.length) {
			return false;
		} else {
			return this.indexOf(search, start) !== -1;
		}
	};
}