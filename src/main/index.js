require('../common/common.js');
require('breakpoints-js/dist/breakpoints.min.js');
require('../numeric-input/_numeric-input.js');
require('slick-carousel');
require('select2');


import './index.scss';


$(function () {

	$('.news-slider').slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		nextArrow: '<div class="news-slider__next"></div>',
  		prevArrow: '<div class="news-slider__prev"></div>',
  		responsive: [
	  		{
	  			breakpoint: 1660,
	  			settings: {
	  				slidesToShow: 2,
	  				slidesToScroll: 2,
	  			}
	  		},
	  		{
	  			breakpoint: 1300,
	  			settings: {
	  				slidesToShow: 1,
	  				slidesToScroll: 1,
	  			}
	  		}
  		]
	});

	$('.news-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		if (nextSlide > currentSlide) {
			$('.news-slider__next').addClass('news-slider__next--active');
		} else {
			$('.news-slider__prev').addClass('news-slider__prev--active');
		}
	});

	$('.news-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
		$('.news-slider__next').removeClass('news-slider__next--active');
		$('.news-slider__prev').removeClass('news-slider__prev--active');
	});

	$('.navi-tabs a').on('shown.bs.tab', function (event) {
		var tabHash = event.currentTarget.hash;
		switch (tabHash) {
			case '#tiresSelectionForm':
				$('.nav-link[href="#tiresSelectionForm"]').parent('.nav-item').addClass('active-tab');
				break;
			case '#rimsSelectionForm':
				$('.nav-link[href="#rimsSelectionForm"]').parent('.nav-item').addClass('active-tab');
				break;
			case '#tiresCalculator':
				$('.nav-link[href="#tiresCalculator"]').parent('.nav-item').addClass('active-tab');
				break;
		}

		$(this).closest('.nav-item').siblings().removeClass('active-tab');
	});


	$('.tires-selection__field select, .tires-selection__field--halfed select').select2({
		minimumResultsForSearch: -1
	});


	

});

