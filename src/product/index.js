require('../common/common.js');
require('breakpoints-js/dist/breakpoints.min.js');
require('../numeric-input/_numeric-input.js');
require('slick-carousel');

import './index.scss';

$(function () {

    $('.product-photos-slider').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: '<div class="product-photos-slider__next"></div>',
        prevArrow: '<div class="product-photos-slider__prev"></div>',
        dots: true,
    });

    $('.navi-tabs a').on('shown.bs.tab', function (event) {
		var tabHash = event.currentTarget.hash;
		switch (tabHash) {
			case '#description':
				$('.nav-link[href="#description"]').parent('.nav-item').addClass('active-tab');
				break;
			case '#existence':
				$('.nav-link[href="#existence"]').parent('.nav-item').addClass('active-tab');
				break;
			case '#suitability':
				$('.nav-link[href="#suitability"]').parent('.nav-item').addClass('active-tab');
                break;
            case '#reviews':
                $('.nav-link[href="#suitability"]').parent('.nav-item').addClass('active-tab');
                break;
		}

		$(this).closest('.nav-item').siblings().removeClass('active-tab');
	});

});
